 _**有问题请加群 320672607**_ 

脚本说明
1.本脚本是可以一键安装AMH+Nginx+php+Mysql。
2.脚本本身集成：PHP5.3、PHP5.4、PHP5.5、PHP5.6、PHP7、MYSQL5.6
3.支持用户任意切换PHP5.3-7以适应网站程序
安装
AMH4.5 一键安装极速编译安装方式 (安装时间10至60分钟)
yum -y install wget && wget http://amh.zhinaonet.com/amh.sh && chmod 775 amh.sh && ./amh.sh 2>&1 | tee amh.log

特点
支持从php5.3~php7.0自由热切换,面板默认使用php5.6
每个虚拟主机可以进行切换，不需要整个环境的切换，部分服务器软件进行了版本升级。
nginx升级到1.10.2稳定版,配合新版nginx,openssl编译1.0.2e
支持http/2
支持stream反代
支持nginx反代和cache_purge
数据库可选择mysql或者maridb
版本信息
Mysql55Version='mysql-5.5.48';
Mysql56Version='mysql-5.6.29';
Mysql57Version='mysql-5.7.11';
Mariadb55Version='mariadb-5.5.47';
Mariadb10Version='mariadb-10.1.11';
Php53Version='php-5.3.29';
Php54Version='php-5.4.45';
Php55Version='php-5.5.38';
Php56Version='php-5.6.27';
Php70Version='php-7.0.12';
NginxVersion='nginx-1.10.2';